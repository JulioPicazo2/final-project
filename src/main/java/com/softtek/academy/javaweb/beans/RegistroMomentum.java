package com.softtek.academy.javaweb.beans;

import java.sql.Date;

public class RegistroMomentum {
	
	int Num;
	String Departament;
	String Name;
	int ID;
	Date DateTime;
	String Verifycode;
	int DeviceID;
	
	public int getNum() {
		return Num;
	}
	public void setNum(int num) {
		Num = num;
	}
	public String getDepartament() {
		return Departament;
	}
	public void setDepartament(String departament) {
		Departament = departament;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public Date getDateTime() {
		return DateTime;
	}
	public void setDateTime(Date dateTime) {
		DateTime = dateTime;
	}
	public String getVerifycode() {
		return Verifycode;
	}
	public void setVerifycode(String verifycode) {
		Verifycode = verifycode;
	}
	public int getDeviceID() {
		return DeviceID;
	}
	public void setDeviceID(int deviceID) {
		DeviceID = deviceID;
	}

}
